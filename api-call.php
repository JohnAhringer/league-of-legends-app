<?php



class apiCall {
	
	const API_KEY = ;
	const APIV1 = 'http://prod.api.pvp.net/api/lol/static-data/{region}/v1/';
	const APIV1_1 = 'http://prod.api.pvp.net/api/lol/{region}/v1.1/';
	const APIV1_2 = 'http://prod.api.pvp.net/api/lol/{region}/v1.2/';
	const APIV1_3 = 'http://prod.api.pvp.net/api/lol/{region}/v1.3/';
	const APIV2_2 = 'http://prod.api.pvp.net/api/lol/{region}/v2.2/';
	const APIV2_3 = 'http://prod.api.pvp.net/api/lol/{region}/v2.3/';
	private $REGION;
	
	function __construct($region) {
		$this->REGION = $region;
	}
	
	function getChampion($data=null) {
		$call = 'champion';
		if ($data == true) {
			$call = self::APIV1_1 . $call . '?freeToPlay=true&';	
		}
		else $call = self::APIV1_1 . $call; 
			
		return $this->request($call);
	}
	
	function getGame($summonerId) {
		$call = 'game/by-summoner/' . $summonerId . '/recent';
		
		$cal = self::APIV1_3 . $call;
		
		return $this->request($call);
	}
	
	function getLeagueChallenger($gameType) {
		$call = 'league/challenger?type=';
		if ($gameType == 'solo') $call = self::APIV2_3 . $call . 'RANKED_SOLO_5x5&';
		elseif ($gameType == 'team_3x3') $call = self::APIV2_3 . $call . 'RANKED_TEAM_3X3&';
		elseif ($gameType == 'team_5x5') $call = self::APIV2_3 . $call . 'RANKED_TEAM_3X3&';
		
		return $this->request($call);
	}
	
	function getLeagueSummonerEntry($summonerId) {
		$call = 'league/by-summoner/' . $summonerId . '/entry';
		
		$call = self::APIV2_3 . $call;
		
		return $this->request($call);
	}
	
	function getLeagueSummoner($summonerId) {
		$call = 'league/by-summoner/' . $summonerId;
		
		$call = self::APIV2_3 . $call;
		
		return $this->request($call);
	}
	
	function getChampList($data=null) {
		$call = 'champion';
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call .'?champData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getChampById($id,$data=null) {
		$call = 'champion/' . $id;
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?champData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getItemList($data=null) {
		$call = 'item';
		
		if($data != '' and $data != null) $call = self::APIV1 . $call . '?itemListData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getItemById($id,$data=null) {
		$call = 'item/' . $id;
		
		if($data != '' and $data != null) $call = self::APIV1 . $call . '?itemData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getMasteryList($data=null) {
		$call = 'mastery';
		
		if($data != '' and $data != null) $call = self::APIV1 . $call . '?masteryListData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getMasteryById($id, $data=null) {
		$call = 'mastery/' . $id;
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?masteryData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getRealm() {
		$call = 'realm';
		
		$call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getRuneList($data=null) {
		$call = 'rune';
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?runeListData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getRuneById($id,$data=null) {
		$call = 'rune/'. $id;
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?runeData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getSummonerSpellList($data=null) {
		$call = 'summoner-spell';
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?spellData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	function getSummonerSpellById($id,$data=null) {
		$call = 'summoner-spell/' . $id;
		
		if ($data != '' and $data != null) $call = self::APIV1 . $call . '?spellData=' . $data . '&';
		else $call = self::APIV1 . $call;
		
		return $this->request($call);
	}
	
	
	
	function request($call) {
	
		$url = $this->formatUrl($call);
	
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return $result;
	}
	
	private function formatUrl($call) {
		if (substr($call, -1) == '&') {
			return str_replace('{region}', $this->REGION, $call) . 'api_key=' . self::API_KEY;
		}
		else {
			return str_replace('{region}',$this->REGION, $call) . '?api_key=' . self::API_KEY;
		}
	}
}	


?>